#include <iostream>
#include <conio.h>
#include <list>
#include <stdlib.h>
using namespace std;


int fib()
{
	int userinput, n, f;
	int collatz[1000];
	int fibonacci[90];
	int a=0;
	int first = 0;
	int second = 1;
	int even = 0;
	
	cout<<"Enter an integer:"<<endl;
	cin>>userinput;
	
	cout<<""<<endl;
	cout<<"Collatz Sequence: "<<endl;
	while(userinput!= 1)
	{
		if (userinput % 2 == 0)
		{
			userinput = userinput/2;
			cout<<userinput<<endl;
			collatz[a] = userinput;
			even++;
		}
		
		else
		{
			userinput = userinput*3 + 1;
			cout<<userinput<<endl;
			collatz[a] = userinput;
		}
		a++;
	}
	
//custom function
	cout<<"Even number in collatz sequence: ";
	cout<<even<<endl;
	cout<<""<<endl;
	
	
//fibonacci.
	for(f=0; f<90; f++)
	{
		fibonacci[f] = first + second;
		first = second;
		second = fibonacci[f];
	}

cout<<"Common numbers in collatz sequence and fibonacci: ";
	for (int a = 0; a <90; a++)
	{		
		for (int f = 0; f <90; f++)
		{
			if (collatz[a] == fibonacci[f])
			{
				cout<<" "<<collatz[a];
			}
		}
	}
	cout<<"\n"<<endl;
return 0;
}

struct Node{
    string data;
    Node *next;
};

void AddItemToList(Node *head);
void DeleteItemFromList(Node *head);
void PrintList(Node *head);

int linklist(){
    bool stop = false;
    int choice;
    Node *head = new Node;
    head->next = NULL;

    while (!stop){
    	cout<<"\n"<<endl;
    	
        cout <<"Press 1. add to list"<<endl;
        cout<< "Press 2. delete from list" << endl;
        cout<< "Press 3. print list" << endl;
        cout<< "Press 4. quit" << endl;
        cin >> choice;
        switch(choice){
        case 1: AddItemToList(head);
            break;
        case 2: DeleteItemFromList(head);
            break;
        case 3: PrintList(head);
            break;
        case 4: stop = true;
            break;
        default:
            cout << "That is not a valid input, quitting program";
            stop = true;
        }
    }
return 0;
}

void AddItemToList(Node *head)
{
	system("CLS");
    bool stop = false;
    string temp;
    Node *current;

    while (!stop){

        cout << "Enter a word('stop' to stop)";
        cin >> temp;

        if (temp == "stop"){
            stop = true;
        }
        else{
            current = new Node;
            current->data = temp;
            current->next = head->next;
            head -> next = current;
        }
    }
    return;
}

void DeleteItemFromList(Node *head)
{
	system("CLS");
    string deletion;
    cout << "Which value do you want to delete from the list? ";
    cin >> deletion;

    Node *prev = head;
    Node *current = head->next;

    while (current)
    {
        if (current->data == deletion){
            prev->next = current->next;
            delete current;
            return;
        }
        prev = current;
        current = current->next;
    }
    if (!current){
        cout << "That value is not in the list" << endl;
    }
}

void PrintList(Node *head)
{
	system("CLS");
    if (!head->next)
    {
        cout << "Nothing is in the list." << endl;
        return;
    }

    Node *current;
    current = head->next;
    while (current)
    {
        cout << current->data << endl;
        current = current->next;
    }
}

int main()
{
	cout<<fib();
	cout<<linklist();
}


